#include <assert.h>
#include <iostream>
#include <sstream>

#include "SFML/Graphics.hpp"
#include "MyDB.h"
#include "Utils.h"

using namespace sf;
using namespace std;

int main()
{
	// Create the main window
	RenderWindow window(VideoMode(1200, 800), "Assessment 4");
	sf::Font font;
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	MyDB mydb;
	mydb.Init("data/chinook.db");

	vector<string> fieldnames;

	DebugPrint("----------------------------------------------------------------------------");

	//Task A
	DebugPrint("Tables in Chinook");

	mydb.ExecQuery("SELECT * FROM sqlite_master WHERE type = 'table'");							//Select everything in file that is a table
	for (size_t i = 0; i < mydb.results.size(); ++i)	
		DebugPrint(mydb.GetStr(i, "name"));														//Print out name of table

	DebugPrint("----------------------------------------------------------------------------");

	//Task B
	DebugPrint("Fields in invoices");

	fieldnames = mydb.GetFieldNames("invoices");												//Get field names in table "invoices"
	for (size_t i = 0; i < fieldnames.size(); ++i)												//Repeat until every fieldname has been printed
		DebugPrint(fieldnames[i]);

	DebugPrint("----------------------------------------------------------------------------");

	//Task C
	DebugPrint("Customer info for Gruber");

	fieldnames = mydb.GetFieldNames("customers");												//Get field names in table "customers"
	mydb.ExecQuery("SELECT * FROM customers where LastName = 'Gruber' ");						//Select Grubers information
	for (size_t i = 0; i < fieldnames.size(); ++i)												//Repeat for each fieldname in table
		DebugPrint(fieldnames[i] + " - " + mydb.GetStr(0, fieldnames[i]));						//Format FIELDNAME - DATA

	DebugPrint("----------------------------------------------------------------------------");

	//Task D
	DebugPrint("Items bought by Gruber");

	//Obtain amount spent & date from each of Gruber's invoices
	mydb.ExecQuery("SELECT invoices.Total, invoices.InvoiceDate FROM customers "\
		"INNER JOIN invoices "\
		"ON customers.CustomerId = invoices.CustomerId "\
		"WHERE customers.LastName = 'Gruber';");
	for (size_t i = 0; i < mydb.results.size(); ++i)
		DebugPrint("�" + (mydb.GetStr(i, "Total")) + " - " + mydb.GetStr(i, "InvoiceDate"));	//Format PRICE - DATE

	//Get total amount Gruber has spent
	mydb.ExecQuery("SELECT SUM(Total) FROM customers " \
		"INNER JOIN invoices "\
		"ON customers.CustomerId = invoices.CustomerId "\
		"WHERE customers.LastName = 'Gruber';");
	DebugPrint("Final total = �" + (mydb.GetStr(0, "SUM(Total)")));

	DebugPrint("----------------------------------------------------------------------------");

	//Task E

	DebugPrint("----------------------------------------------------------------------------");
	mydb.SaveToDisk();
	mydb.Close();

	Clock clock;
	// Start the game loop 
	while (window.isOpen())
	{
		bool fire = false;
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
		} 

		// Clear screen
		window.clear();

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();

		sf::Text txt("Hello world", font, 30);
		window.draw(txt);


		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
